from random import choice

from kivy.core.image import Image
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty


class BaseWidget(Widget):
    def load_tileable(self, name):
        t = Image(f"images/background/{name[1]}.png").texture
        t.wrap = "repeat"
        setattr(self, f"tx_{name[0]}", t)


class Background(BaseWidget):
    tx_space = ObjectProperty(None)
    tx_stars = ObjectProperty(None)

    def create(self):
        bg_images = {"space": "space1", "stars": "stars1"}
        bg_images["space"] = choice(["space1", "space2", "space3"])
        bg_images["stars"] = choice(["stars1", "stars2"])
        for item in bg_images.items():
            self.load_tileable(item)

    def set_background_size(self, tx):
        tx.uvsize = (1, self.height / tx.height)

    def on_size(self, *args):
        for tx in (self.tx_space, self.tx_stars):
            self.set_background_size(tx)

    def set_background_uv(self, name, val):
        t = getattr(self, name)
        t.uvpos = (t.uvpos[0], (t.uvpos[1] - val) % self.height)
        self.property(name).dispatch(self)

    def update(self, fps):
        self.set_background_uv("tx_space", 0.1 * fps)
        self.set_background_uv("tx_stars", 0.1 * fps)
