from random import randint, choice
from functools import partial

from kivymd.app import MDApp
from kivymd.uix.screen import MDScreen
from kivymd.uix.floatlayout import MDFloatLayout
from kivymd.uix.button import MDFlatButton, MDRaisedButton
from kivymd.uix.dialog import MDDialog
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.widget import Widget
from kivy.core.window import Window
from kivy.core.text import LabelBase
from kivy.clock import Clock
from kivy.animation import Animation
from kivy.core.audio import SoundLoader
from kivy.lang import Builder
from kivy.metrics import sp, dp
from kivy.properties import (
    NumericProperty,
    ObjectProperty,
    BooleanProperty,
    StringProperty,
    ListProperty,
    DictProperty,
)

from background import Background
from objects import Spaceship, Bullet, Enemy, Explosion
from dialog import MyMDDialog
from reward_scr import RewardScreen
from hangar_scr import HangarScreen
from loading_screen import LoadingScreen
from space_scr import SpaceScreen
from settings_scr import SettingsScreen
from io_data import save, load

Builder.load_file("objects.kv")
Builder.load_file("progressbar.kv")
Builder.load_file("reward_scr.kv")
Builder.load_file("hangar_scr.kv")
Builder.load_file("space_scr.kv")
Builder.load_file("settings_scr.kv")
Builder.load_file("loading_screen.kv")

NUMBER_OF_FRAMES = 40
VERSION = "v.1.0.0"


class Game(ScreenManager):
    player_health = NumericProperty(0)
    player_spaceship = NumericProperty(0)
    stopwatch_label = StringProperty("00:00")
    enemy_type = NumericProperty(0)
    enemies_shooting_speed = NumericProperty(2)
    stage_completed = BooleanProperty(False)
    coins = NumericProperty(20)
    crystals = NumericProperty(0)
    level = NumericProperty(1)
    rewards = DictProperty()
    sound_on = BooleanProperty(True)
    music_on = BooleanProperty(True)
    sound_list = ListProperty([])
    music_list = ListProperty([])
    main_theme_play = BooleanProperty(False)
    pause_btn_disable = BooleanProperty(False)
    music_loaded = BooleanProperty(False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.create_rewards_dict()
        saves_player = load("saves_player")
        if len(saves_player) != 0:
            self.player_spaceship = saves_player["spaceship"]
            self.coins = saves_player["coins"]
            self.crystals = saves_player["crystals"]

        self.sound_btn = SoundLoader.load("sounds/btn_press.wav")
        self.sound_chest = SoundLoader.load("sounds/chest_open.wav")
        self.sound_laser = SoundLoader.load("sounds/laser-gun-shot.wav")
        self.sound_impact = SoundLoader.load("sounds/impact.wav")
        self.sound_list = [
            self.sound_btn,
            self.sound_chest,
            self.sound_laser,
            self.sound_impact,
        ]
        self.music_list = []

        saves_settings = load("saves_settings")
        if len(saves_settings) != 0:
            self.music_on = saves_settings["music_on"]
            self.sound_on = saves_settings["sound_on"]

        self.sound_mute_unmute()
        self.music_mute_unmute()

    def load_music(self, dt):
        self.music_game_win = SoundLoader.load("sounds/game_win.ogg")
        self.music_list.append(self.music_game_win)
        self.music_game_lose = SoundLoader.load("sounds/game_lose.ogg")
        self.music_list.append(self.music_game_lose)
        self.music_battle = SoundLoader.load("sounds/battle.ogg")
        self.music_list.append(self.music_battle)
        self.music_main_theme = SoundLoader.load("sounds/cosmic-glow-6703.ogg")
        self.music_list.append(self.music_main_theme)
        
    def sound_mute_unmute(self):
        if self.sound_on:
            for sound in self.sound_list:
                sound.volume = 1
        else:
            for sound in self.sound_list:
                sound.volume = 0

    def music_mute_unmute(self):
        if self.music_on:
            for music in self.music_list:
                music.volume = 1
        else:
            for music in self.music_list:
                music.volume = 0

    def stop_sound(self):
        for sound in self.sound_list:
            sound.stop()
            
    def levels_manage(self):
        if self.level == 1:
            self.enemy_type = 0
            self.enemies_shooting_speed = 2
        if self.level == 2:
            self.enemy_type = 0
            self.enemies_shooting_speed = 1.5
        if self.level == 3:
            self.enemy_type = 0
            self.enemies_shooting_speed = 1
        if self.level == 4:
            self.enemy_type = 1
            self.enemies_shooting_speed = 2
        if self.level == 5:
            self.enemy_type = 1
            self.enemies_shooting_speed = 1.5
        if self.level == 6:
            self.enemy_type = 1
            self.enemies_shooting_speed = 1
        if self.level == 7:
            self.enemy_type = 2
            self.enemies_shooting_speed = 2
        if self.level == 8:
            self.enemy_type = 2
            self.enemies_shooting_speed = 1.5
        if self.level == 9:
            self.enemy_type = 2
            self.enemies_shooting_speed = 1

    def create_rewards_dict(self):
        self.rewards[1] = [100, 0, 200, 0, 300, 0]
        self.rewards[2] = [200, 0, 300, 0, 400, 0]
        self.rewards[3] = [0, 1, 0, 2, 0, 4]
        self.rewards[4] = [400, 0, 500, 0, 600, 0]
        self.rewards[5] = [500, 0, 600, 0, 700, 0]
        self.rewards[6] = [0, 2, 0, 3, 0, 6]
        self.rewards[7] = [700, 0, 800, 0, 900, 0]
        self.rewards[8] = [800, 0, 900, 0, 1000, 0]
        self.rewards[9] = [0, 3, 0, 4, 0, 8]


class Lottery(MDFloatLayout):
    pass
    
    
class HomeScreen(MDScreen):
    bottom_navigation = ObjectProperty(None)
    lottery_dialog = None
    enter_screen = BooleanProperty(False)

    def on_enter(self):
        self.bottom_navigation.switch_tab("space")
        
        if not self.enter_screen:
            self.animate_test_your_luck_label()
            event = Clock.schedule_interval(self.animate_star_1, 3)
            event2 = Clock.schedule_interval(self.animate_star_2, 4)
            event3 = Clock.schedule_interval(self.animate_star_3, 5)
            event4 = Clock.schedule_interval(self.animate_star_4, 4.5)
            self.enter_screen = True
        if not self.game.main_theme_play:
            self.game.music_main_theme.loop = True
            self.game.music_main_theme.play()
            self.game.main_theme_play = True

    def play_btn_press(self):
        self.game.sound_btn.play()
        self.game.current = "space_scr"

    def switch_to_hangar_scr(self):
        self.game.current = "hangar_scr"
        self.game.sound_btn.play()
        
    def switch_to_settings_scr(self):
        self.game.current = "settings_scr"
        self.game.sound_btn.play()

    def animate_star_1(self, dt):
        anim = Animation(size=(60, 60), duration=0.15)
        anim += Animation(size=(20, 20), duration=0.15)
        anim.start(self.ids.star_pulse_1)
        
    def animate_star_2(self, dt):
        anim = Animation(size=(60, 60), duration=0.15)
        anim += Animation(size=(20, 20), duration=0.15)
        anim.start(self.ids.star_pulse_2)
        
    def animate_star_3(self, dt):
        anim = Animation(size=(60, 60), duration=0.15)
        anim += Animation(size=(20, 20), duration=0.15)
        anim.start(self.ids.star_pulse_3)
        
    def animate_star_4(self, dt):
        anim = Animation(size=(60, 60), duration=0.15)
        anim += Animation(size=(20, 20), duration=0.15)
        anim.start(self.ids.star_pulse_4)
               
    def show_lottery_dialog(self):
        if not self.lottery_dialog:
            self.lottery_dialog = MDDialog(
                radius=[36, 36, 36, 36],
                width_offset=dp(48),
                md_bg_color="#282a2b",
                type="custom",
                auto_dismiss=False,
                content_cls=Lottery(),
                buttons=[
                    MDFlatButton(
                        text="CLOSE",
                        font_name="Rabbit",
                        theme_text_color="Custom",
                        text_color=(1, 1, 1, 1),
                        on_release=lambda _: self.lottery_dialog_close_btn(),
                    ),
                ],
            )
        self.game.sound_btn.play()
        self.lottery_dialog.content_cls.ids.lottery_image.source = (
            "images/chest_close.png"
        )
        self.lottery_dialog.content_cls.ids.lottery_reward_layout.opacity = 0
        if self.game.coins < 20:
            self.lottery_dialog.content_cls.ids.lottery_btn.disabled = True
        else:
            self.lottery_dialog.content_cls.ids.lottery_btn.disabled = False
        self.lottery_dialog.open()
        
    def lottery_dialog_close_btn(self):
        self.game.sound_btn.play()
        self.lottery_dialog.dismiss()
        
    def get_prize_value(self):
        number = randint(1, 100)
        if number in [1, 51, 99]:
            crystals = 1
            coins = 0
        elif number in [15, 35, 55, 75, 95]:
            crystals = 0
            coins = 100
        elif number % 10 == 0:
            crystals = 0
            coins = 50
        else:
            crystals = 0
            coins = 10
        return [crystals, coins]
        
    def get_prize(self):
        self.game.coins -= 20
        prize = self.get_prize_value()
        self.game.crystals += prize[0]
        self.game.coins += prize[1]
        save(
            "saves_player",
            spaceship=self.game.player_spaceship,
            coins=self.game.coins,
            crystals=self.game.crystals,
        )
        self.lottery_dialog.content_cls.ids.lottery_image.source = (
            "images/chest_open.png"
        )
        if prize[0] > 0:
            self.lottery_dialog.content_cls.ids.lottery_reward_image = "images/crystal.png"
            self.lottery_dialog.content_cls.ids.lottery_reward_label.text = f"{prize[0]}"
        else:
            self.lottery_dialog.content_cls.ids.lottery_reward_label.text = f"{prize[1]}"
        self.lottery_dialog.content_cls.ids.lottery_reward_layout.opacity = 1
        self.lottery_dialog.content_cls.ids.lottery_btn.disabled = True
        self.game.sound_chest.play()
        
    def animate_test_your_luck_label(self):
        anim = Animation(color=(1, 1, 1, 0.5), duration=2) + Animation(color=(1, 1, 1, 1), duration=2)
        anim.repeat = True
        anim.start(self.ids.test_your_luck_label)
        
                                                              
class GameScreen(Screen):
    game_widget = ObjectProperty(None)
    pause_dialog = None

    def on_pre_enter(self):
        self.game_widget.player.type_ = self.game.player_spaceship
        self.game_widget.player.create()
        self.game_widget.bg.create()
        self.game_widget.on_start_reset()

    def on_enter(self):
        self.game_widget.animate_start_label()
        self.game.music_main_theme.stop()
        self.game.main_theme_play = False
        self.game.music_battle.loop = True
        self.game.music_battle.play()
        
    def on_leave(self):
        self.game.music_battle.stop()
        self.game.stop_sound()

    def get_pause_dialog(self):
        if not self.pause_dialog:
            self.pause_dialog = MyMDDialog(
                auto_dismiss=False,
                radius=[36, 36, 36, 36],
                md_bg_color="2F406C",
                icon="pause-circle-outline",
                btn_pos="center",
                buttons=[
                    MDRaisedButton(
                        text="exit",
                        font_name="fonts/ethnocentric_rg.otf",
                        md_bg_color="DC8C1B",
                        on_release=lambda _: self.exit_btn_press(),
                    ),
                    MDRaisedButton(
                        text="Resume",
                        font_name="fonts/ethnocentric_rg.otf",
                        md_bg_color="1C88E0",
                        font_size="10sp",
                        on_release=lambda _: self.resume_btn_press(),
                    ),
                ],
            )
        self.pause_dialog.open()

    def press_pause_btn(self):
        for event in self.game_widget.events:
            event.cancel()
        self.get_pause_dialog()

    def resume_btn_press(self):
        self.pause_dialog.dismiss()
        for event in self.game_widget.events:
            event()
            
    def exit_btn_press(self):
        self.pause_dialog.dismiss()
        self.game_widget.reset()
        self.game.current = "home_scr"
                    
class GameWidget(Widget):
    screen_size_x = NumericProperty(0)
    screen_size_y = NumericProperty(0)
    bg = ObjectProperty(None)
    player = ObjectProperty(None)
    player_bullets = ListProperty([])
    pause_btn = ObjectProperty(None)
    spaceship_icon = ObjectProperty(None)
    enemies = ListProperty([])
    enemies_bullets = ListProperty([])
    enemy_start_x = NumericProperty(0)
    enemy_start_y = NumericProperty(0)
    enemy_size = NumericProperty(0)
    game_started = BooleanProperty(False)
    sw_seconds = NumericProperty(0)
    game_widgets = ListProperty([])

    def __init__(self, **kwargs):
        super(GameWidget, self).__init__(**kwargs)
        self.screen_size_x, self.screen_size_y = Window.size
        self.enemy_size = self.screen_size_x / 8 - 10

    def animate_player(self):
        anim = Animation(y=self.height * 0.1, duration=1)
        anim.start(self.player)
        
    def animate_start_label(self):
        AliensApp.game.pause_btn_disable = True
        self.draw_enemies()
        self.spaceship_icon.image = self.player.image
        anim = Animation(opacity=1, duration=2) + Animation(opacity=0, duration=2)
        anim.bind(on_complete=lambda a, w: self.start())
        anim.start(self.ids.start_label)

    def start(self):
        self.animate_player()
        event = Clock.schedule_interval(self.update, 1 / NUMBER_OF_FRAMES)
        event2 = Clock.schedule_interval(self.player_shoot, 1 / 2)
        event3 = Clock.schedule_interval(
            self.enemy_shoot, AliensApp.game.enemies_shooting_speed
        )
        event4 = Clock.schedule_interval(self.update_time, 1)
        self.events = [
            event,
            event2,
            event3,
            event4,
        ]
        AliensApp.game.pause_btn_disable = False

    def update(self, fps):
        self.bg.update(fps)

        for bullet in self.player_bullets:
            bullet.move(fps)
            if bullet.y > (self.height + bullet.height):
                self.remove_widget(bullet)
                self.player_bullets.remove(bullet)

        for enemy in self.enemies:
            enemy.move()
            enemy.update(fps)
            if enemy.y <= 0:
                self.player.health = 0

        for bullet in self.enemies_bullets:
            bullet.move(fps)
            if bullet.y <= -bullet.height:
                self.remove_widget(bullet)
                self.enemies_bullets.remove(bullet)

        self.check_collisions()
        if self.game_over():
            self.get_reward_scr()

    def on_touch_move(self, touch):
        self.player.center_x = touch.x

    def update_time(self, nap):
        if self.game_started:
            self.sw_seconds += nap
            minutes, seconds = divmod(self.sw_seconds, 60)
            AliensApp.game.stopwatch_label = "%02d:%02d" % (
                int(minutes),
                int(seconds),
            )

    def player_shoot(self, dt):
        bullet = Bullet(
            self.player.center_x - 10,
            self.player.y + self.player.height,
            self.player.bullet_type,
        )
        self.add_widget(bullet)
        self.player_bullets.append(bullet)
        AliensApp.game.sound_laser.play()

    def draw_enemies(self):
        self.enemy_start_x = self.screen_size_x / 8
        self.enemy_start_y = self.screen_size_y * 0.5 + self.enemy_size
        for i in range(6):
            for j in range(4):
                enemy = Enemy(
                    self.enemy_start_x + (self.enemy_size + 20) * i,
                    self.enemy_start_y + (self.enemy_size + 20) * j,
                    AliensApp.game.enemy_type,
                )
                enemy.size_ = [
                    self.enemy_size,
                ] * 2
                self.add_widget(enemy)
                self.enemies.append(enemy)

    def enemy_shoot(self, dt):
        ex_shooting_enemy = 0
        if len(self.enemies) >= 3:
            for i in range(3):
                shooting_enemy = randint(0, (len(self.enemies) - 1))
                if shooting_enemy != ex_shooting_enemy:
                    bullet = Bullet(
                        self.enemies[shooting_enemy].center_x - 12,
                        self.enemies[shooting_enemy].y,
                        "enemy",
                    )
                    self.add_widget(bullet)
                    self.enemies_bullets.append(bullet)
                    ex_shooting_enemy = shooting_enemy

    def get_blast(self, exp_x, exp_y, exp_size, widget):
        exp = Explosion(exp_x, exp_y, exp_size)
        self.add_widget(exp)

        def change_explosion(img, dt):
            exp.image = exp.images_list[img]

        for i in range(11):
            Clock.schedule_once(partial(change_explosion, i), (0.2 + i * 0.06))

        anim = Animation(opacity=0.95, duration=0.6)
        anim.bind(on_complete=lambda a, w: self.remove_widget(exp))
        anim.start(exp)

    def check_collisions(self):
        for enemy in self.enemies:
            for bullet in self.player_bullets:
                if enemy.collide_point(bullet.x, bullet.y):
                    self.remove_widget(bullet)
                    self.player_bullets.remove(bullet)
                    enemy.health -= bullet.damage
                    if enemy.health <= 0:
                        self.get_blast(
                            enemy.x,
                            enemy.y,
                            [
                                self.enemy_size,
                            ]
                            * 2,
                            enemy,
                        )
                        self.remove_widget(enemy)
                        self.enemies.remove(enemy)
                        AliensApp.game.sound_impact.play()

            if enemy.collide_widget(self.player):
                self.player.health = 0

        for bullet in self.enemies_bullets:
            if self.player in self.children and bullet.collide_widget(self.player):
                self.remove_widget(bullet)
                self.enemies_bullets.remove(bullet)
                self.player.health -= bullet.damage

    def game_over(self):
        if self.player.health <= 0 or len(self.enemies) == 0:
            return True
        else:
            return False

    def reset(self):
        for widget in self.children:
            if widget not in [
                self.bg,
                self.player,
                self.spaceship_icon,
                self.ids.stopwatch,
                self.ids.progress_bar,
                self.ids.start_label,
            ]:
                self.game_widgets.append(widget)
        self.clear_widgets(children=self.game_widgets)

    def get_reward_scr(self):
        for event in self.events:
            event.cancel()
        self.reset()
        if self.player.health <= 0:
            AliensApp.game.stage_completed = False
        if len(self.enemies) == 0:
            AliensApp.game.stage_completed = True
            AliensApp.game.level += 1
        AliensApp.game.current = "reward_scr"

    def on_start_reset(self):
        self.player.center_x = self.center_x
        self.player.y = -self.player.height
        self.game_started = True
        self.sw_seconds = 0
        self.enemies.clear()
        AliensApp.game.stopwatch_label = "00:00"


class AliensApp(MDApp):
    version = StringProperty("")

    def build(self):
        self.version = VERSION
        self.theme_cls.theme_style = "Dark"

        AliensApp.game = Game(transition=FadeTransition())
        self.game.add_widget(LoadingScreen(name="loading_scr"))
        self.game.add_widget(HomeScreen(name="home_scr"))
        self.game.add_widget(SpaceScreen(name="space_scr"))
        self.game.add_widget(HangarScreen(name="hangar_scr"))
        self.game.add_widget(GameScreen(name="game_scr"))
        self.game.add_widget(RewardScreen(name="reward_scr"))
        self.game.add_widget(SettingsScreen(name="settings_scr"))
        return self.game


if __name__ == "__main__":
    LabelBase.register(
        name="Rabbit",
        fn_regular="fonts/ethnocentric_rg.otf",
        fn_bold="fonts/ethnocentric_rg.otf",
        fn_italic="fonts/ethnocentric_rg.otf",
    )
    AliensApp().run()
