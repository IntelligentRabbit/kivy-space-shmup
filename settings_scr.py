from kivymd.uix.screen import MDScreen
from kivy.properties import ObjectProperty

from io_data import save


class SettingsScreen(MDScreen):
    bottom_navigation = ObjectProperty(None)

    def on_enter(self):
        self.bottom_navigation.switch_tab("settings")

    def sound_switch(self):
        if self.ids.sound_switch.active:
            self.game.sound_on = True
        else:
            self.game.sound_on = False
        self.game.sound_mute_unmute()
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
        )

    def music_switch(self):
        if self.ids.music_switch.active:
            self.game.music_on = True
        else:
            self.game.music_on = False
        self.game.music_mute_unmute()
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
        )

    def switch_to_hangar_scr(self):
        self.game.current = "hangar_scr"
        self.game.sound_btn.play()

    def switch_to_home_scr(self):
        self.game.current = "home_scr"
        self.game.sound_btn.play()
