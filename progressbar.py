__all__ = ("MyMDProgressBar",)

import os
from typing import NoReturn

from kivy.animation import Animation
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import (
    BooleanProperty,
    ColorProperty,
    NumericProperty,
    OptionProperty,
    StringProperty,
)
from kivy.uix.progressbar import ProgressBar

from kivymd.theming import ThemableBehavior


class MyMDProgressBar(ThemableBehavior, ProgressBar):
    reversed = BooleanProperty(False)

    orientation = OptionProperty("horizontal", options=["horizontal", "vertical"])
    """
    Orientation of progressbar. Available options are: `'horizontal '`,
    `'vertical'`.
    """

    color = ColorProperty(None)

    running_transition = StringProperty("in_cubic")

    catching_transition = StringProperty("out_quart")

    running_duration = NumericProperty(0.5)

    catching_duration = NumericProperty(0.8)

    type = OptionProperty(
        None, options=["indeterminate", "determinate"], allownone=True
    )
    """
    Type of progressbar. Available options are: `'indeterminate '`,
    `'determinate'`.
    """

    _x = NumericProperty(0)

    def __init__(self, **kwargs):
        self.catching_anim = None
        self.running_anim = None
        super().__init__(**kwargs)

    def start(self) -> NoReturn:
        """Start animation."""

        if self.type in ("indeterminate", "determinate"):
            Clock.schedule_once(self._set_default_value)
            if not self.catching_anim and not self.running_anim:
                if self.type == "indeterminate":
                    self._create_indeterminate_animations()
                else:
                    self._create_determinate_animations()
            self.running_away()

    def stop(self) -> NoReturn:
        """Stop animation."""

        Animation.cancel_all(self)
        self._set_default_value(0)

    def running_away(self, *args) -> NoReturn:
        self._set_default_value(0)
        self.running_anim.start(self)

    def catching_up(self, *args) -> NoReturn:
        if self.type == "indeterminate":
            self.reversed = True
        self.catching_anim.start(self)

    def _create_determinate_animations(self):
        self.running_anim = Animation(
            value=100,
            opacity=1,
            t=self.running_transition,
            d=self.running_duration,
        )
        self.running_anim.bind(on_complete=self.catching_up)
        self.catching_anim = Animation(
            opacity=0,
            t=self.catching_transition,
            d=self.catching_duration,
        )
        self.catching_anim.bind(on_complete=self.running_away)

    def _create_indeterminate_animations(self):
        self.running_anim = Animation(
            _x=self.width / 2,
            value=50,
            t=self.running_transition,
            d=self.running_duration,
        )
        self.running_anim.bind(on_complete=self.catching_up)
        self.catching_anim = Animation(
            value=0, t=self.catching_transition, d=self.catching_duration
        )
        self.catching_anim.bind(on_complete=self.running_away)

    def _set_default_value(self, interval):
        self._x = 0
        self.value = 0
        self.reversed = False
