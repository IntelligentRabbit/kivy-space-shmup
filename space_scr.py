from kivymd.uix.screen import MDScreen
from kivy.animation import Animation
from kivymd.uix.card import MDCard
from kivymd.uix.floatlayout import MDFloatLayout
from kivy.properties import (
    NumericProperty,
    StringProperty,
    ObjectProperty,
    BooleanProperty,
    ListProperty,
)

from io_data import load


class Level(MDFloatLayout):
    image = StringProperty("")
    image_down = StringProperty("")
    level_number = NumericProperty(0)
    disabled = BooleanProperty(True)
    star_1 = BooleanProperty(False)
    star_2 = BooleanProperty(False)
    star_3 = BooleanProperty(False)
    stars = NumericProperty(0)

    def open_stars(self):
        if self.stars == 1:
            self.star_1 = True
        if self.stars == 2:
            self.star_1 = True
            self.star_2 = True
        if self.stars == 3:
            self.star_1 = True
            self.star_2 = True
            self.star_3 = True


class SpaceScreen(MDScreen):
    level_1 = ObjectProperty(None)
    level_2 = ObjectProperty(None)
    level_3 = ObjectProperty(None)
    level_4 = ObjectProperty(None)
    level_5 = ObjectProperty(None)
    level_6 = ObjectProperty(None)
    level_7 = ObjectProperty(None)
    level_8 = ObjectProperty(None)
    level_9 = ObjectProperty(None)

    def on_pre_enter(self):
        self.levels = [
            self.level_1,
            self.level_2,
            self.level_3,
            self.level_4,
            self.level_5,
            self.level_6,
            self.level_7,
            self.level_8,
            self.level_9,
        ]

        saves_levels = load("saves_levels")
        if len(saves_levels) != 0:
            self.level_1.disabled = saves_levels["level1_disabled"]
            self.level_1.stars = saves_levels["level1_stars"]
            self.level_2.disabled = saves_levels["level2_disabled"]
            self.level_2.stars = saves_levels["level2_stars"]
            self.level_3.disabled = saves_levels["level3_disabled"]
            self.level_3.stars = saves_levels["level3_stars"]
            self.level_4.disabled = saves_levels["level4_disabled"]
            self.level_4.stars = saves_levels["level4_stars"]
            self.level_5.disabled = saves_levels["level5_disabled"]
            self.level_5.stars = saves_levels["level5_stars"]
            self.level_6.disabled = saves_levels["level6_disabled"]
            self.level_6.stars = saves_levels["level6_stars"]
            self.level_7.disabled = saves_levels["level7_disabled"]
            self.level_7.stars = saves_levels["level7_stars"]
            self.level_8.disabled = saves_levels["level8_disabled"]
            self.level_8.stars = saves_levels["level8_stars"]
            self.level_9.disabled = saves_levels["level9_disabled"]
            self.level_9.stars = saves_levels["level9_stars"]

        for level in self.levels:
            level.open_stars()

    def level_btn_press(self, level_number):
        self.game.level = level_number
        self.game.levels_manage()
        self.game.current = "game_scr"
        self.game.sound_btn.play()

    def return_btn_press(self):
        self.game.current = "home_scr"
        self.game.sound_btn.play()
