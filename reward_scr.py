from kivymd.uix.screen import MDScreen
from kivy.animation import Animation
from kivy.metrics import sp, dp
from kivy.properties import NumericProperty, StringProperty

from io_data import save


class RewardScreen(MDScreen):
    stage_result = NumericProperty(0)
    height_result = NumericProperty(30)  # seconds
    low_result = NumericProperty(60)
    reward_image = StringProperty("")
    reward_coins = NumericProperty(0)
    reward_crystals = NumericProperty(0)

    def on_enter(self):
        if self.game.stage_completed:
            self.game.music_game_win.play()
        else:
            self.game.music_game_lose.play()
        self.animate_result_label()

    def on_leave(self):
        self.ids.star_1.opacity = 0
        self.ids.star_2.opacity = 0
        self.ids.star_3.opacity = 0
        self.ids.stage.pos_hint = {"center_x": 0}
        self.ids.sw_label.pos_hint = {"center_x": 1}
        self.ids.result.font_size = sp(10)
        self.ids.reward_layout.opacity = 0
        self.ids.btn_replay.pos_hint = {"top": 0}
        self.ids.btn_next.pos_hint = {"top": 0}
        self.ids.reward_label.text = ""
        self.reward_coins = 0
        self.reward_crystals = 0
        self.ids.return_btn.disabled = True
        self.game.music_game_win.stop()
        self.game.music_game_lose.stop()

    def animate_reward_layout(self):
        if self.game.stage_completed:
            anim = Animation(opacity=1, duration=0.5)
            if self.reward_coins > 0:
                self.reward_image = "images/coin.png"
                self.ids.reward_label.text = f"{self.reward_coins}"
            else:
                self.reward_image = "images/crystal.png"
                self.ids.reward_label.text = f"{self.reward_crystals}"
            self.game.coins += self.reward_coins
            self.game.crystals += self.reward_crystals
            save(
                "saves_player",
                spaceship=self.game.player_spaceship,
                coins=self.game.coins,
                crystals=self.game.crystals,
            )
            anim.start(self.ids.reward_layout)
        self.ids.return_btn.disabled = False

        anim_btn_replay = Animation(pos_hint={"top": 0.3}, duration=0.4)
        anim_btn_replay.start(self.ids.btn_replay)

        anim_btn_next = Animation(pos_hint={"top": 0.28}, duration=0.4)
        anim_btn_next.start(self.ids.btn_next)

    def animate_star_box(self):
        anim = Animation(size=(dp(132), dp(40)), duration=0.2)
        anim.bind(on_complete=lambda a, w: self.animate_reward_layout())
        anim.start(self.ids.star_box)

        self.stage_result = self.game.get_screen("game_scr").game_widget.sw_seconds
        anim_star_1 = Animation(opacity=1, duration=0.5)
        anim_star_2 = Animation(opacity=1, duration=0.5)
        anim_star_3 = Animation(opacity=1, duration=0.5)
        if self.game.stage_completed:
            self.game.get_screen("space_scr").levels[
                self.game.level - 1
            ].disabled = False
            if self.stage_result <= self.height_result:
                self.game.get_screen("space_scr").levels[self.game.level - 2].stars = 3
                self.reward_coins = self.game.rewards[self.game.level - 1][4]
                self.reward_crystals = self.game.rewards[self.game.level - 1][5]
                anim_star_1.start(self.ids.star_1)
                anim_star_2.start(self.ids.star_2)
                anim_star_3.start(self.ids.star_3)

            elif self.height_result < self.stage_result <= self.low_result:
                self.game.get_screen("space_scr").levels[
                    (self.game.level - 2)
                ].stars = 2
                self.reward_coins = self.game.rewards[self.game.level - 1][2]
                self.reward_crystals = self.game.rewards[self.game.level - 1][3]
                anim_star_1.start(self.ids.star_1)
                anim_star_2.start(self.ids.star_2)

            elif self.stage_result > self.low_result:
                self.game.get_screen("space_scr").levels[
                    (self.game.level - 2)
                ].stars = 1
                self.reward_coins = self.game.rewards[self.game.level - 1][0]
                self.reward_crystals = self.game.rewards[self.game.level - 1][1]
                anim_star_1.start(self.ids.star_1)

            save(
                "saves_levels",
                level1_disabled=self.game.get_screen("space_scr").levels[0].disabled,
                level1_stars=self.game.get_screen("space_scr").levels[0].stars,
                level2_disabled=self.game.get_screen("space_scr").levels[1].disabled,
                level2_stars=self.game.get_screen("space_scr").levels[1].stars,
                level3_disabled=self.game.get_screen("space_scr").levels[2].disabled,
                level3_stars=self.game.get_screen("space_scr").levels[2].stars,
                level4_disabled=self.game.get_screen("space_scr").levels[3].disabled,
                level4_stars=self.game.get_screen("space_scr").levels[3].stars,
                level5_disabled=self.game.get_screen("space_scr").levels[4].disabled,
                level5_stars=self.game.get_screen("space_scr").levels[4].stars,
                level6_disabled=self.game.get_screen("space_scr").levels[5].disabled,
                level6_stars=self.game.get_screen("space_scr").levels[5].stars,
                level7_disabled=self.game.get_screen("space_scr").levels[6].disabled,
                level7_stars=self.game.get_screen("space_scr").levels[6].stars,
                level8_disabled=self.game.get_screen("space_scr").levels[7].disabled,
                level8_stars=self.game.get_screen("space_scr").levels[7].stars,
                level9_disabled=self.game.get_screen("space_scr").levels[8].disabled,
                level9_stars=self.game.get_screen("space_scr").levels[8].stars,
            )

    def animate_sw_label(self):
        anim = Animation(pos_hint={"center_x": 0.7}, duration=0.4)
        anim.bind(on_complete=lambda a, w: self.animate_star_box())
        anim.start(self.ids.sw_label)

    def animate_stage_label(self):
        anim = Animation(pos_hint={"center_x": 0.3}, duration=0.4)
        anim.bind(on_complete=lambda a, w: self.animate_sw_label())
        anim.start(self.ids.stage)

    def animate_result_label(self):
        anim = Animation(font_size=sp(40), duration=0.4)
        anim += Animation(font_size=sp(36), duration=0.4)
        anim.bind(on_complete=lambda a, w: self.animate_stage_label())
        anim.start(self.ids.result)

    def next_btn_press(self):
        self.game.levels_manage()
        self.game.current = "game_scr"
        self.game.sound_btn.play()

    def replay_btn_press(self):
        self.game.current = "game_scr"
        self.game.sound_btn.play()

    def return_btn_press(self):
        self.game.current = "home_scr"
        self.game.sound_btn.play()
