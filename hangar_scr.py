from kivymd.uix.screen import MDScreen
from kivy.animation import Animation
from kivymd.uix.card import MDCard
from kivy.properties import (
    NumericProperty,
    StringProperty,
    ObjectProperty,
    BooleanProperty,
)

from io_data import save, load


class SpaceshipShowcase(MDCard):
    image = StringProperty("")
    currency_image = StringProperty("")
    spaceship_price = NumericProperty(0)
    in_use = BooleanProperty(False)
    bought = BooleanProperty(False)


class HangarScreen(MDScreen):
    showcase_1 = ObjectProperty(None)
    showcase_2 = ObjectProperty(None)
    showcase_3 = ObjectProperty(None)
    shields = NumericProperty(200)
    damage = NumericProperty(100)
    bullets_speed = NumericProperty(700)
    bottom_navigation = ObjectProperty(None)
    main_showcase_animated = BooleanProperty(False)

    def on_enter(self):
        self.bottom_navigation.switch_tab("hangar")
        saves_ships = load("saves_ships")
        if len(saves_ships) != 0:
            self.showcase_1.bought = saves_ships["ship1_bought"]
            self.showcase_1.in_use = saves_ships["ship1_in_use"]
            self.showcase_2.bought = saves_ships["ship2_bought"]
            self.showcase_2.in_use = saves_ships["ship2_in_use"]
            self.showcase_3.bought = saves_ships["ship3_bought"]
            self.showcase_3.in_use = saves_ships["ship3_in_use"]
        self.set_main_showcase()
        if not self.main_showcase_animated:
            self.animate_main_showcase()
            self.main_showcase_animated = True

    def set_main_showcase(self):
        if self.showcase_1.in_use:
            self.ids.main_showcase.source = "images/spaceships/spaceship-1x.png"
            self.shields = 200
            self.damage = 100
            self.bullets_speed = 700
        if self.showcase_2.in_use:
            self.ids.main_showcase.source = "images/spaceships/spaceship-2x.png"
            self.shields = 300
            self.damage = 200
            self.bullets_speed = 800
        if self.showcase_3.in_use:
            self.ids.main_showcase.source = "images/spaceships/spaceship-3x.png"
            self.shields = 400
            self.damage = 300
            self.bullets_speed = 900
            
    def animate_main_showcase(self):
        anim = Animation(pos_hint={"center_x": 0.5, "top": 0.96}, duration=2.6)
        anim += Animation(pos_hint={"center_x": 0.5, "top": 0.93}, duration=2.6)
        anim.repeat = True
        anim.start(self.ids.main_showcase)

    def showcase_1_press(self):
        self.showcase_1.in_use = True
        self.showcase_2.in_use = False
        self.showcase_3.in_use = False
        self.set_main_showcase()
        self.game.player_spaceship = 0
        save(
            "saves_ships",
            ship1_bought=self.showcase_1.bought,
            ship1_in_use=self.showcase_1.in_use,
            ship2_bought=self.showcase_2.bought,
            ship2_in_use=self.showcase_2.in_use,
            ship3_bought=self.showcase_3.bought,
            ship3_in_use=self.showcase_3.in_use,
        )
        save(
            "saves_player",
            spaceship=self.game.player_spaceship,
            coins=self.game.coins,
            crystals=self.game.crystals,
        )
        self.game.sound_btn.play()

    def showcase_2_press(self):
        if not self.showcase_2.bought:
            if self.game.coins >= self.showcase_2.spaceship_price:
                self.game.coins -= self.showcase_2.spaceship_price
                self.showcase_2.bought = True
        else:
            self.showcase_2.in_use = True
            self.showcase_1.in_use = False
            self.showcase_3.in_use = False
            self.set_main_showcase()
            self.game.player_spaceship = 1
        save(
            "saves_ships",
            ship1_bought=self.showcase_1.bought,
            ship1_in_use=self.showcase_1.in_use,
            ship2_bought=self.showcase_2.bought,
            ship2_in_use=self.showcase_2.in_use,
            ship3_bought=self.showcase_3.bought,
            ship3_in_use=self.showcase_3.in_use,
        )
        save(
            "saves_player",
            spaceship=self.game.player_spaceship,
            coins=self.game.coins,
            crystals=self.game.crystals,
        )
        self.game.sound_btn.play()

    def showcase_3_press(self):
        if not self.showcase_3.bought:
            if self.game.crystals >= self.showcase_3.spaceship_price:
                self.game.crystals -= self.showcase_3.spaceship_price
                self.showcase_3.bought = True
        else:
            self.showcase_3.in_use = True
            self.showcase_1.in_use = False
            self.showcase_2.in_use = False
            self.set_main_showcase()
            self.game.player_spaceship = 2
        save(
            "saves_ships",
            ship1_bought=self.showcase_1.bought,
            ship1_in_use=self.showcase_1.in_use,
            ship2_bought=self.showcase_2.bought,
            ship2_in_use=self.showcase_2.in_use,
            ship3_bought=self.showcase_3.bought,
            ship3_in_use=self.showcase_3.in_use,
        )
        save(
            "saves_player",
            spaceship=self.game.player_spaceship,
            coins=self.game.coins,
            crystals=self.game.crystals,
        )
        self.game.sound_btn.play()

    def switch_to_home_scr(self):
        self.game.current = "home_scr"
        self.game.sound_btn.play()

    def switch_to_settings_scr(self):
        self.game.current = "settings_scr"
        self.game.sound_btn.play()
