from random import randint

from kivy.uix.image import Image as ImageWidget
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.widget import Widget
from kivy.properties import (
    StringProperty,
    NumericProperty,
    ListProperty,
    BooleanProperty,
    OptionProperty,
)
from kivymd.uix.button import MDIconButton
from kivy.metrics import dp


class Spaceship(ImageWidget):
    type_ = NumericProperty(0)
    image = StringProperty("")
    health = NumericProperty(0)
    max_health = NumericProperty(0)
    bullet_type = StringProperty("")

    def create(self):
        for i in range(3):
            if self.type_ == i:
                self.bullet_type = f"player-{i+1}"
                self.image = f"images/spaceships/spaceship-{i+1}x.png"
                self.max_health = 200 + 100 * i
                self.health = self.max_health

        if self.type_ == 0:
            self.size = (102, 88)
        if self.type_ == 1:
            self.size = (102, 88)
        if self.type_ == 2:
            self.size = (102, 88)


class Bullet(ImageWidget):
    image = StringProperty("")
    speed = NumericProperty(0)
    damage = NumericProperty(0)

    def __init__(self, x, y, type_):
        super(Bullet, self).__init__()
        self.x = x
        self.y = y
        self.type_ = type_
        if self.type_ == "player-1":
            self.image = "images/bullets/1.png"
            self.size = (20, 40)
            self.speed = 700
            self.damage = 100

        if self.type_ == "player-2":
            self.image = "images/bullets/2.png"
            self.size = (20, 40)
            self.speed = 800
            self.damage = 200

        if self.type_ == "player-3":
            self.image = "images/bullets/3.png"
            self.size = (20, 40)
            self.speed = 900
            self.damage = 300

        if self.type_ == "enemy":
            self.image = "images/bullets/4.png"
            self.size = (24, 24)
            self.speed = -500
            self.damage = 100

    def move(self, fps):
        self.y += self.speed * fps


class Enemy(ImageWidget):
    type_ = NumericProperty(0)
    image = StringProperty()
    images = ListProperty([])
    current_image = NumericProperty(0)
    size_ = ListProperty([80, 80])
    health = NumericProperty(0)
    max_health = NumericProperty(0)
    not_moving = BooleanProperty(True)
    start_pos_x = NumericProperty(0)
    x_speed = NumericProperty(1)
    y_speed = NumericProperty(0)
    count_direction_x_changes = NumericProperty(0)

    def __init__(self, x, y, type_):
        super(Enemy, self).__init__()
        self.x = x
        self.y = y
        self.type_ = type_
        for i in range(3):
            if self.type_ == i:
                self.max_health = 200 + i * 200
                self.health = self.max_health
                self.image = f"images/enemies/enemy-{i+1}/frame-1.png"

                images_pt1 = [f"images/enemies/enemy-{i+1}/frame-1.png"] * 10
                images_pt2 = [f"images/enemies/enemy-{i+1}/frame-2.png"] * 10
                self.images = images_pt1 + images_pt2

    def move(self):
        if self.not_moving:
            self.start_pos_x = self.x
            self.not_moving = False
        self.x += self.x_speed
        self.y += self.y_speed
        if self.x <= (self.start_pos_x - self.size_[0] / 2) or self.x >= (
            self.start_pos_x + self.size_[0] / 2
        ):
            self.x_speed *= -1
            self.count_direction_x_changes += 1
        if self.count_direction_x_changes < 3:
            self.y_speed = 0
        else:
            self.y_speed = -self.size_[1] / 2
            self.count_direction_x_changes = 0

    def update(self, dt):
        self.current_image = (self.current_image + 1) % 20
        self.image = self.images[self.current_image]


class SpaceshipIcon(Widget):
    image = StringProperty("")


class Explosion(ImageWidget):
    image = StringProperty("")
    images_list = ListProperty()
    y_speed = NumericProperty(0)

    def __init__(self, x, y, size):
        super(Explosion, self).__init__()
        self.x = x
        self.y = y
        self.size = size
        for i in range(11):
            self.image = f"images/explosion/{i+1}.png"
            self.images_list.append(self.image)


class PauseButton(MDIconButton):
    pass
